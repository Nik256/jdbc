package com.epam.online_auction;

import com.epam.online_auction.demo.DemoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineAuctionApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineAuctionApplication.class, args)
                .getBean(DemoService.class)
                .execute();
    }

}

