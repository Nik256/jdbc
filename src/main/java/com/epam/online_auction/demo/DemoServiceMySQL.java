package com.epam.online_auction.demo;

import com.epam.online_auction.dao.BidDao;
import com.epam.online_auction.dao.ItemDao;
import com.epam.online_auction.dao.UserDao;
import com.epam.online_auction.dto.Item;
import com.epam.online_auction.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DemoServiceMySQL implements DemoService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BidDao bidDao;

    @Autowired
    private ItemDao itemDao;

    @Override
    public void execute() {
        System.out.println("1. Список ставок данного пользователя");
        System.out.println(bidDao.findBidsByUserId(1L));
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("2. Список лотов данного пользователя");
        System.out.println(itemDao.findItemsByUserId(1L));
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("3. Поиск лотов по подстроке в названии");
        System.out.println(itemDao.findItemsByName("картина"));
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("4. Поиск лотов по подстроке в описании");
        System.out.println(itemDao.findItemsByDescription("Франция"));
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("5. Средняя цена лотов каждого пользователя");
        System.out.println(itemDao.getAvgItemsPrices());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("6. Максимальный размер имеющихся ставок на каждый лот");
        System.out.println(bidDao.getMaxBidPriceForEachItem());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("7. Список действующих лотов данного пользователя");
        System.out.println(itemDao.getActiveItemsByUserId(3L));
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("8. Добавить нового пользователя");
        User user = new User(
                0,
                "Linus Torvalds",
                "Helsinki, Finland",
                "linus3923jkUdf4J6d",
                "H7sdslJ7dfhsfh2d");
        System.out.println(user);
        userDao.createUser(user);
        System.out.println("Users:\n" + userDao.findAllUsers());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("9. Добавить новый лот");
        Item item = new Item(
                0,
                "One Ring",
                "The One Ring was forged by the Dark Lord Sauron during the Second Age " +
                        "to gain dominion over the free peoples of Middle-earth.",
                99999999,
                1,
                LocalDate.of(2555, 3, 22),
                LocalDate.of(3453, 1, 2),
                false,
                2);
        System.out.println(item);
        itemDao.createItem(item);
        System.out.println("Items:\n" + itemDao.findAllItems());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("10. Удалить ставки данного пользователя");
        System.out.println("User id = 2");
        bidDao.deleteBidsByUserId(2L);
        System.out.println("Bids:\n" + bidDao.findAllBids());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("11. Удалить лоты данного пользователя");
        System.out.println("User id = 3");
        itemDao.deleteItemsByUserId(3L);
        System.out.println("Items:\n" + itemDao.findAllItems());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("12. Увеличить стартовые цены товаров данного пользователя вдвое");
        System.out.println("User id = 2");
        itemDao.doubleStartPriceOfItemsByUserId(2L);
        System.out.println("Items:\n" + itemDao.findAllItems());
        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("13. Получить список лотов, торги которыми ещё не начались");
        System.out.println(itemDao.getItemsThatHaveNotStartedTrading());
        System.out.println("----------------------------------------------------------------------------------------");
    }
}
