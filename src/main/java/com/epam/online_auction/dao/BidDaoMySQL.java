package com.epam.online_auction.dao;

import com.epam.online_auction.dto.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class BidDaoMySQL implements BidDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Bid> rowMapper = (rs, rowNum) -> new Bid(
            rs.getInt("bid_id"),
            rs.getDate("bid_date").toLocalDate(),
            rs.getInt("bid_value"),
            rs.getInt("items_item_id"),
            rs.getInt("users_user_id")
    );

    @Override
    public List<Bid> findBidsByUserId(Long userId) {
        String sql = "SELECT * FROM bids WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, rowMapper);
    }

    @Override
    public List<Map<String, Object>> getMaxBidPriceForEachItem() {
        String sql = "SELECT items_item_id, MAX(bid_value) FROM bids GROUP BY items_item_id";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Bid> findAllBids() {
        String sql = "SELECT * FROM bids";
        return jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public void deleteBidsByUserId(Long userId) {
        String sql = "DELETE FROM bids WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }
}
