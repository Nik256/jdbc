package com.epam.online_auction.dao;

import com.epam.online_auction.dto.Item;

import java.util.List;
import java.util.Map;

public interface ItemDao {

    List<Item> findItemsByUserId(Long userId);

    List<Item> findItemsByName(String name);

    List<Item> findItemsByDescription(String description);

    List<Map<String, Object>> getAvgItemsPrices();

    List<Item> getActiveItemsByUserId(Long userId);

    void createItem(Item item);

    List<Item> findAllItems();

    void deleteItemsByUserId(Long userId);

    void doubleStartPriceOfItemsByUserId(Long userId);

    List<Item> getItemsThatHaveNotStartedTrading();
}
