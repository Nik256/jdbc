package com.epam.online_auction.dao;

import com.epam.online_auction.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ItemDaoMySQL implements ItemDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Item> rowMapper = (rs, rowNum) -> new Item(
            rs.getInt("item_id"),
            rs.getString("title"),
            rs.getString("description"),
            rs.getDouble("start_price"),
            rs.getDouble("bid_increment"),
            rs.getDate("start_date").toLocalDate(),
            rs.getDate("stop_date").toLocalDate(),
            rs.getBoolean("by_it_now"),
            rs.getInt("users_user_id")
    );

    @Override
    public List<Item> findItemsByUserId(Long userId) {
        String sql = "SELECT * FROM items WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, rowMapper);
    }

    @Override
    public List<Item> findItemsByName(String name) {
        String sql = "SELECT * FROM items WHERE title LIKE ?";
        return jdbcTemplate.query(sql, new Object[]{"%" + name + "%"}, rowMapper);
    }

    @Override
    public List<Item> findItemsByDescription(String description) {
        String sql = "SELECT * FROM items WHERE description LIKE ?";
        return jdbcTemplate.query(sql, new Object[]{"%" + description + "%"}, rowMapper);
    }

    @Override
    public List<Map<String, Object>> getAvgItemsPrices() {
        String sql = "SELECT users_user_id, AVG(start_price) FROM items GROUP BY users_user_id";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Item> getActiveItemsByUserId(Long userId) {
        String sql = "SELECT * FROM items WHERE users_user_id = ? AND (CURDATE() BETWEEN start_date AND stop_date)";
        return jdbcTemplate.query(sql, new Object[]{userId}, rowMapper);
    }

    @Override
    public void createItem(Item item) {
        String sql = "INSERT INTO items (title, description, start_price, bid_increment, start_date, stop_date, users_user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                item.getTitle(),
                item.getDescription(),
                item.getStartPrice(),
                item.getBidIncrement(),
                item.getStartDate(),
                item.getStopDate(),
                item.getUsersUserId()
        );
    }

    @Override
    public List<Item> findAllItems() {
        String sql = "SELECT * FROM items";
        return jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public void deleteItemsByUserId(Long userId) {
        String sql = "DELETE FROM bids WHERE items_item_id IN (" +
                "SELECT item_id FROM items WHERE users_user_id = ?)";
        jdbcTemplate.update(sql, userId);

        sql = "DELETE FROM items WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }

    @Override
    public void doubleStartPriceOfItemsByUserId(Long userId) {
        String sql = "UPDATE items SET start_price = start_price * 2 WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }

    @Override
    public List<Item> getItemsThatHaveNotStartedTrading() {
        String sql = "SELECT * FROM items WHERE start_date > CURDATE()";
        return jdbcTemplate.query(sql, rowMapper);
    }
}