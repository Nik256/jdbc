package com.epam.online_auction.dao;

import com.epam.online_auction.dto.Bid;

import java.util.List;
import java.util.Map;

public interface BidDao {

    List<Bid> findBidsByUserId(Long userId);

    List<Map<String, Object>> getMaxBidPriceForEachItem();

    List<Bid> findAllBids();

    void deleteBidsByUserId(Long userId);
}
