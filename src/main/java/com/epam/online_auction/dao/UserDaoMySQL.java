package com.epam.online_auction.dao;

import com.epam.online_auction.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoMySQL implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> rowMapper = (rs, rowNum) -> new User(
            rs.getInt("user_id"),
            rs.getString("full_name"),
            rs.getString("billing_address"),
            rs.getString("login"),
            rs.getString("password")
    );

    @Override
    public void createUser(User user) {
        String sql = "INSERT INTO users (full_name, billing_address, login, password) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                user.getFullName(),
                user.getBillingAddress(),
                user.getLogin(),
                user.getPassword()
        );
    }

    @Override
    public List<User> findAllUsers() {
        String sql = "SELECT * FROM users";
        return jdbcTemplate.query(sql, rowMapper);
    }
}
