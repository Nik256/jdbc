package com.epam.online_auction.dao;

import com.epam.online_auction.dto.User;

import java.util.List;

public interface UserDao {

    void createUser(User user);

    List<User> findAllUsers();
}
