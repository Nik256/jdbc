package com.epam.online_auction.dto;

import java.time.LocalDate;

public class Bid {
    private long id;
    private LocalDate bidDate;
    private double bidValue;
    private long itemsItemId;
    private long usersUserId;

    public Bid(long id,
               LocalDate bidDate,
               double bidValue,
               long itemsItemId,
               long usersUserId) {
        this.id = id;
        this.bidDate = bidDate;
        this.bidValue = bidValue;
        this.itemsItemId = itemsItemId;
        this.usersUserId = usersUserId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getBidDate() {
        return bidDate;
    }

    public void setBidDate(LocalDate bidDate) {
        this.bidDate = bidDate;
    }

    public double getBidValue() {
        return bidValue;
    }

    public void setBidValue(double bidValue) {
        this.bidValue = bidValue;
    }

    public long getItemsItemId() {
        return itemsItemId;
    }

    public void setItemsItemId(long itemsItemId) {
        this.itemsItemId = itemsItemId;
    }

    public long getUsersUserId() {
        return usersUserId;
    }

    public void setUsersUserId(long usersUserId) {
        this.usersUserId = usersUserId;
    }

    @Override
    public String toString() {
        return "\nBid{" +
                "id=" + id +
                ", bidDate=" + bidDate +
                ", bidValue=" + bidValue +
                ", itemsItemId=" + itemsItemId +
                ", usersUserId=" + usersUserId +
                "}\n";
    }
}
