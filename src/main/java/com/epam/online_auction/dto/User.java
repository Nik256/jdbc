package com.epam.online_auction.dto;

public class User {
    private long id;
    private String fullName;
    private String billingAddress;
    private String login;
    private String password;

    public User(long id,
                String fullName,
                String billingAddress,
                String login,
                String password) {
        this.id = id;
        this.fullName = fullName;
        this.billingAddress = billingAddress;
        this.login = login;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "\nUser{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", billingAddress='" + billingAddress + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                "}\n";
    }
}
