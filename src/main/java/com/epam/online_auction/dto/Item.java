package com.epam.online_auction.dto;

import java.time.LocalDate;

public class Item {
    private long id;
    private String title;
    private String description;
    private double startPrice;
    private double bidIncrement;
    private LocalDate startDate;
    private LocalDate stopDate;
    private boolean byItNow;
    private long usersUserId;

    public Item(long id,
                String title,
                String description,
                double startPrice,
                double bidIncrement,
                LocalDate startDate,
                LocalDate stopDate,
                boolean byItNow,
                long usersUserId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.startPrice = startPrice;
        this.bidIncrement = bidIncrement;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.byItNow = byItNow;
        this.usersUserId = usersUserId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getBidIncrement() {
        return bidIncrement;
    }

    public void setBidIncrement(double bidIncrement) {
        this.bidIncrement = bidIncrement;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getStopDate() {
        return stopDate;
    }

    public void setStopDate(LocalDate stopDate) {
        this.stopDate = stopDate;
    }

    public boolean isByItNow() {
        return byItNow;
    }

    public void setByItNow(boolean byItNow) {
        this.byItNow = byItNow;
    }

    public long getUsersUserId() {
        return usersUserId;
    }

    public void setUsersUserId(long usersUserId) {
        this.usersUserId = usersUserId;
    }

    @Override
    public String toString() {
        return "\nItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startPrice=" + startPrice +
                ", bidIncrement=" + bidIncrement +
                ", startDate=" + startDate +
                ", stopDate=" + stopDate +
                ", byItNow=" + byItNow +
                ", usersUserId=" + usersUserId +
                "}\n";
    }


}
